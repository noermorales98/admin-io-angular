# Admin

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.5.

### Nombre del proyecto: Admin.io

Se trata de una aplicación web enfocada a grupos de trabajo, en donde el usuario administrador pueda crear grupos de trabajo y administrar al personal al que está a cargo.

**Un usuario administrador:**

puede crear, agregar, editar y eliminar grupos de trabajo o colaboradores.

**Grupo de trabajo:**

Solo un usuario admin podrá hacer CRUD de este grupo.
El grupo contendrá información sobre: tareas a realizar, notas, pendientes, avisos.

**Usuarios colaboradores:**

Serán capaces de ver el contenido del grupo, marcando la lista con las tareas ya realizadas.

**Usuarios no registrados:**

Verán la pagina de inicio y la información de esta, pero no podrán acceder a ningún tipo de información
